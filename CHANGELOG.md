
## 0.0.10 [08-31-2024]

deprecate the Pre-Built

See merge request itentialopensource/pre-built-automations/versa-create-device-group!10

2024-08-31 17:26:26 +0000

---

## 0.0.9 [05-24-2023]

* Merging pre-release/2023.1 into master to run cypress tests

See merge request itentialopensource/pre-built-automations/versa-create-device-group!9

---

## 0.0.8 [03-03-2023]

* Updated Readme

See merge request itentialopensource/pre-built-automations/versa-create-device-group!8

---

## 0.0.7 [02-14-2022]

* Certify on 2021.2

See merge request itentialopensource/pre-built-automations/versa-create-device-group!4

---

## 0.0.6 [07-06-2021]

* Update package.json, README.md files

See merge request itentialopensource/pre-built-automations/versa-create-device-group!3

---

## 0.0.5 [05-25-2021]

* Update package.json

See merge request itentialopensource/pre-built-automations/staging/versa-create-device-group!2

---

## 0.0.4 [03-22-2021]

* Update package.json

See merge request itentialopensource/pre-built-automations/staging/versa-create-device-group!2

---

## 0.0.3 [03-22-2021]

* Patch/dsup 904 readme

See merge request itentialopensource/pre-built-automations/staging/versa-create-device-group!1

---

## 0.0.2 [03-09-2021]

* updated the template to replace the word Artifact

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!3

---

## 0.0.6 [10-15-2020]

* updated the template to replace the word Artifact

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!3

---

## 0.0.5 [08-03-2020]

* Fixed repository.url in Package.json

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!2

---

## 0.0.4 [07-17-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---

## 0.0.3 [07-07-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---

## 0.0.2 [06-19-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---\n\n
