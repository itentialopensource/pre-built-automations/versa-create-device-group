## _Deprecation Notice_
This Pre-Built has been deprecated as of 09-01-2024 and will be end of life on 09-01-2025. The capabilities of this Pre-Built have been replaced by the [Versa - REST](https://gitlab.com/itentialopensource/pre-built-automations/versa-rest)

<!-- This is a comment in md (Markdown) format, it will not be visible to the end user -->

<!-- Update the below line with your Pre-Built name -->
# Versa Create Device Group

<!-- Leave TOC intact unless you've added or removed headers -->
## Table of Contents

* [Overview](#overview)
* [Requirements](#requirements)
* [Features](#features)
* [How to Install](#how-to-install)
* [How to Run](#how-to-run)
* [Additional Information](#additional-information)

## Overview

This pre-built is used to add a new device group to Versa Director. Device group is a group of all devices that need to share the same configuration (belonging to a template) such as the SD-WAN policies, the FW rules, QoS markings, etc.
<!-- Write a few sentences about the Pre-Built and explain the use case(s) -->
<!-- Avoid using the word Artifact. Please use Pre-Built, Pre-Built Transformation or Pre-Built Automation -->
<!-- Ex.: The Migration Wizard enables IAP users to conveniently move their automation use cases between different IAP environments -->
<!-- (e.g. from Dev to Pre-Production or from Lab to Production). -->

<!-- Workflow(s) Image Placeholder - TO BE ADDED DIRECTLY TO GitLab -->
<!-- REPLACE COMMENT BELOW WITH IMAGE OF YOUR MAIN WORKFLOW -->
<!--

-->
<!-- ADD ESTIMATED RUN TIME HERE -->
<!-- e.g. Estimated Run Time: 34 min. -->
_Estimated Run Time_: 1 minute

## Requirements

This Pre-Built requires the following:

<!-- Unordered list highlighting the requirements of the Pre-Built -->
<!-- EXAMPLE -->
<!-- * cisco ios device -->
<!-- * Ansible or NSO (with F5 NED) * -->
* Itential Automation Platform
  * `^2022.1`
* A running instance of the Itential OpenSource Versa Director adapter, which can be installed from [here](https://gitlab.com/itentialopensource/adapters/controller-orchestrator/adapter-versa_director).

## Features

The main benefits and features of the Pre-Built are outlined below.

Every Itential Pre-built is designed to optimize network performance and configuration by focusing on reliability, flexibility and coordination of network changes with applications and IT processes. As the network evolves, Pre-builts allow customers to focus on effective management and compliance to minimize risks of disruption and outage that can negatively impact quality of service.

<!-- Unordered list highlighting the most exciting features of the Pre-Built -->
<!-- EXAMPLE -->
<!-- * Automatically checks for device type -->
<!-- * Displays dry-run to user (asking for confirmation) prior to pushing config to the device -->
<!-- * Verifies downloaded file integrity (using md5), will try to download again if failed -->


## How to Install

* Verify you are running a supported version of the Itential Automation Platform (IAP) as listed above in the [Requirements](#requirements) section in order to install the Pre-Built. 
* The Pre-Built can be installed from within App-Admin_Essential. Simply search for the name of your desired Pre-Built and click the install button (as shown below).

<!-- OPTIONAL - Explain if external components are required outside of IAP -->
<!-- Ex.: The Ansible roles required for this Pre-Built can be found in the repository located at https://gitlab.com/itentialopensource/pre-built-automations/hello-world -->

## How to Run

Use the following to run the Pre-Built:


The pre-built can be run as a standalone or as part of a parent job. 
- To run it as a standalone job, run the respective job from operations manager with the necessary json form. 
- The workflow or the job can also be used as part of another parent job.

The description of the formdata needed by the prebuilt is given below:
The required values to be passed on are:

**Device Group Name:** Name of the device group to be created.\
**Organization Name:** Name of the parent organization for which the device group needs to be created.\
**CA In Data Center:** To enable certificate authority. If enabled, during global ZTP, the CSR request goes over the IPSec tunnel, and the branch receives a CA-signed certificate. All tunnels that are established use this certificate.\
**URL Based ZTP:** Will need to be selected if the ZTP process needs be URL Based. When selected, the CPE device will be enabled to provision via a URL that will be generated and sent to an admin who can use that to onboard the device.\
**Enable 2-factor Authentication** (Enable 2 factor to access the device group): To be enabled if 2 factor authentication is to be used during the Zero Touch Provisioning process. When the tunnel is established, before the Orchestrator is notified, an email and/or SMS will be sent to the administrator listed to authorize connectivity to the HeadEnd.\
**Email and Phone:** Email and Phone number of the Administrator. Will be used for notification during ZTP, to provide 2-factor authentication.

**Post staging template name:** Name of the Post staging template that will be used for onboarding the devices in this device group.\
**Controller+VPN profile:** Name of the Controller and VPN Profile that will need to be used during URL based ZTP process.
<!-- Explain the main entrypoint(s) for this Pre-Built: Automation Catalog item, Workflow, Postman, etc. -->

## Versa instance

The above created service template can be verified on the versa instance by logging in using the credentials provided.

After logging in, select the configuration tab, choose the right organization from the left pane and from the Devices dropdown on the top menu choose 'Device Groups' to view all the device groups created.

## Additional Information

Please use your Itential Customer Success account if you need support when using this Pre-Built.
